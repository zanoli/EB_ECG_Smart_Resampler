import os
import pickle
import sys
from time import time

import matplotlib.pyplot as plt
import numpy as np
import pandas as pd

scripts = '../helper_scripts'
if scripts not in sys.path:
    sys.path.insert(0,scripts)
import multiprocessing
import shutil
from multiprocessing import Pool, Process

from csvManager import csvManager
import extract_data
import ECG_lvlCrossing
import ECG_beats_division



if __name__ == "__main__":
    import argparse
    parser = argparse.ArgumentParser()
    parser.add_argument("--cores", help="Force used number of cores (default, half of the available ones")
    parser.add_argument("--num_files", help="Nuimber of files to extract, if no argument is given, all files are pre-processed")
    args = parser.parse_args()

    present_folders = filter(lambda name: False if "dataRaw" in name else True, os.listdir("../data"))
    for directory in present_folders:
        shutil.rmtree(os.path.join("../data",directory))

    if args.cores is not None:
        used_cores = int(args.cores)
    else:
        used_cores = multiprocessing.cpu_count()//2
    print(f"Pre-processing data with {used_cores} cores...")
    start_t_0 = time()
    extract_data.process(cores=used_cores)
    stop_t_0 = time()
    ECG_beats_division.process(cores = used_cores)
    stop_t_1 = time()

    delta_t_0 =   stop_t_0-start_t_0
    delta_t_1 =   stop_t_1-stop_t_0
    delta_t_all = stop_t_1 - start_t_0

    print("\n\n ---------------------------------------------------------------------------------------------------------------------\n")
    print (f"Elapsed time for extraction, using {used_cores} cores: {int(delta_t_0//60)}:{int((delta_t_0-delta_t_0//60*60)//1)}")
    print (f"Elapsed time for ECG beats subdivision, using {used_cores} cores: {int(delta_t_1//60)}:{int((delta_t_1-delta_t_1//60*60)//1)}")
    print (f"Elapsed total time for pre processing, using {used_cores} cores: {int(delta_t_all//60)}:{int((delta_t_all-delta_t_all//60*60)//1)}")
    
