"""
Created on Fri Feb 22 09:07:30 2019

Simple script to perform a non-uniform subsampling of an ECG record in the MIT-BIH format, using
the Wall-Danielsson algorithm. As output, it produces a 2-column csv file with the sample times
and values.

Dependencies:
    - numpy
    - tqdm (https://pypi.org/project/tqdm/)
    - wfdb-python (https://pypi.org/project/wfdb/)

@authors: T. Teijeiro S.Zanoli
"""

import multiprocessing
import os
import shutil
import sys
from multiprocessing import Pool, Process

import numpy as np
import pandas as pd
from tqdm import trange

scripts = '../helper_scripts'
if scripts not in sys.path:
    sys.path.insert(0,scripts)
from time import time

import matplotlib.pyplot as plt
from csvManager import csvManager
import pickle

'''
with open(file_name_full,"rb") as f:
        data = pickle.load(f)

file_out = os.path.join(out_dir,file_name.split(".")[0]+".pickle")
with open(file_out, 'wb') as handle:
    pickle.dump(data_inverted_keys, handle, protocol=pickle.HIGHEST_PROTOCOL)
'''

data_folder = "../data/extracted_data"
out = "../data/level_crossing"
log_dir = "../data/logs/"
pos = False
bits_data = 11
bits = range(1,bits_data+1,1)
hister = 5


def ADC(beat, nBits, hist = 5, original_bits = 11):
    #ADC stats
    delta = 2**original_bits
    dV = (delta)/(2**nBits)
    hist = hist/100*dV
    min_val = -delta//2
    events = {'t':[],'v':[], 'QRS_pos': beat['QRS_pos']}
    #init value, first sample (we assume we always sample the nearest level at start) and ADC status
    v_0 = beat['v'][0]
    lowTh = min_val+((v_0-min_val)//dV)*dV
    highTh = lowTh + dV
    events['t'].append(beat['t'][0])
    events['v'].append(int(lowTh if v_0-lowTh < highTh - v_0 else highTh))

    for val,time in zip(beat['v'],beat['t']):
        #print(f"Value: {val}, time: {time}, low_th = {lowTh - hist}, high_th = { highTh + hist}")
        if val > highTh + hist or val < lowTh - hist:
            direction = 1 if val > highTh else -1
            lowTh = min_val+((val-min_val)//dV)*dV #Delta from the bottom: (val-min_val)//dV*dV then compute the actual level summin min_val
            highTh = lowTh + dV
            events['t'].append(time)
            events['v'].append(int(lowTh if direction == 1 else highTh))
    return events
def sample(file):
    '''
    manager = csvManager()
    _,vals = manager.read(file)
    '''
    with open(file,"rb") as f:
        data = pickle.load(f)
    vals = data['v']
    
    print("------------------------- Sub-sampling file: {} -------------------------".format(file))
    for b in bits:
        #print("\nFile: {}, subsampling with {} bits".format(file,b))
        idxs, vals_samp= ADC(vals, original_bits = bits_data, all_pos = pos, nBits = b, hist = hister)
        compression = 1-len(vals_samp)/len(vals)

        #---------- SAVE BACK ----------
        out_dir = os.path.join(out,str(b)+"bits")
        file_name = os.path.join(out_dir,os.path.basename(file))
        data_out = {'t':idxs,'v':vals_samp}
        with open(file_name, 'wb') as handle:
            pickle.dump(data_out, handle, protocol=pickle.HIGHEST_PROTOCOL)
        #manager.write(idxs,vals_samp,file_name)
        log(file_name,b,compression)

def log (source_file, bits, compression):
    name = os.path.basename(source_file).split('.')[0]
    file_name = os.path.join(log_dir,str(bits)+"Bits")
    str_to_write = name + ": "+str(compression)+"\n"
    with open(file_name,"a") as f:
        f.write(str_to_write)
    
def log_resume():
    resume = {}
    if not os.path.isdir(log_dir):
        return
    for l in os.listdir(log_dir):
        if "resume" in l:
            continue
        bits = int(l[0:l.find("Bits")])
        resume[bits] = {"avg":None,"std":None, "num_files":None}
        compressions = []
        text = ""
        num_file = 0
        with open(os.path.join(log_dir,l)) as f:
            text = f.readlines()
        for line in text:
            num_file += 1
            compr = float(line[line.find(": ")+len(": "):])
            compressions.append(compr)
        resume[bits]["avg"] = np.average(compressions)
        resume[bits]["std"] = np.std(compressions)
        resume[bits]["num_files"] = num_file
    with open(os.path.join(log_dir,"resume.txt"),"w") as f:
        keys = sorted(list(resume.keys()))
        for k in keys:
            line = "Bits: {}\t\tAvg: {}\tStD: {} (Total number of files:{})\n".format(str(k),resume[k]["avg"],resume[k]["std"],resume[k]["num_files"])
            f.write(line)

def process(multi = True,cores = 1):
    names = [os.path.join(data_folder,name) for name in os.listdir(data_folder)]
    if os.path.isdir(log_dir):
        shutil.rmtree(log_dir)
    os.mkdir(log_dir)
    if os.path.isdir(out):
        shutil.rmtree(out)
    os.mkdir(out)
    for b in bits:
        out_dir = os.path.join(out,str(b)+"bits")
        os.mkdir(out_dir)
    if multi:
        print(f"Executing level-crossing EB_sampling with {cores} cores...")
        used_cores = cores
        with Pool(used_cores) as pool:
            pool.map(sample, names)

    else:
        for arg in names:
            sample(arg)
    log_resume()

def test_performances():
    times = []
    print("~"*85)
    print("Analyzing multicore performances in sampling signals with level crossing algorithm and saving in binary.\n"
          "Usefull? No, interesting, yes.")
    print("-"*85)
    print("-"*85)
    print("\nChecking single core (no library used)\n")
    start = time()
    process(multi=False)
    stop = time()
    print (f"    Elapsed time for single core: {int((stop - start)//60)}:{int(((stop-start)-(stop - start)//60*60)//1)}\n\n")
    print("Using multicores...")
    for core_num in range(1,multiprocessing.cpu_count()):
        print("-"*85)
        print(f"Using {core_num} cores...")
        start = time()
        process(multi=True, cores=core_num)
        stop = time()
        times.append(start-stop)
        print (f"    Elapsed time using {core_num} cores: {int((stop - start)//60)}:{int(((stop-start)-(stop - start)//60*60)//1)}\n\n")
    plt.figure()
    plt.plot(times)
    plt.savefig("../data/logs/perf.png")

if __name__ == "__main__":
    import argparse
    parser = argparse.ArgumentParser()
    parser.add_argument("--test", help="test multicore capabilities",
                        action="store_true")
    parser.add_argument("--cores", help="Force used number of cores (default, half of the available ones")
    args = parser.parse_args()
    if args.test:
        print("TEST MULTICORE...")
        test_performances()
    else:
        if args.cores is not None:
            used_cores = int(args.cores)
        else:
            used_cores = multiprocessing.cpu_count()//2
        process(multi=True, cores=used_cores)
