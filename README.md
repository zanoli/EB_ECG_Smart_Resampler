This is the main repository for the smart resampler project.
The data for this repository can be fond here: [Dataset](https://physionet.org/content/nsrdb/1.0.0/)

To run this project:
- Make this directory the home directory for this project
- Download the [Dataset](https://physionet.org/content/nsrdb/1.0.0/) and put it inside a "./data/dataRaw" folder
- Execute:
    ```cd ./pre_proc```
    ```python3 pre_process.py [--cores used_cores]```
    NOTE: by default, half of the cores will be used, the performed operations might be memory intensive so we suggest to use two or three cores
- To check DTW results on an ECG beat run:
    ```cd  ../src```
    ```python3 DTW_alligment.py [--file file_name][--beat beat_to_analyze (sequential)]```

used: 
    - [DTIDistance](https://github.com/wannesm/dtaidistance) for DTW
    - [WFDB](https://pypi.org/project/wfdb/)
    - [eventBased](https://c4science.ch/source/eb_lib/)
