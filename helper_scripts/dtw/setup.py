from distutils.core import setup
from distutils.extension import Extension
from Cython.Build import cythonize
import numpy as np 

# definitions
sourcefiles = ['dtw.pyx','cdtw.c']
extensions = [Extension('dtw', sourcefiles, include_dirs=[np.get_include()])]

# setup function
setup(name='dtw', ext_modules=cythonize(extensions))
