"""
Created on 29.01.2020

Module with definition of classes to work with custom bynary files representing a csv in differential representation (time).

@author: S. Zanoli
"""

import numpy as np
import os

LEN_TIME_FIELDS = 3
LEN_VLUE_FIELDS = 2
LEN_TAG = 4
LEN_META_SECTION_SIZE = 5

class csvManager(object):
    def __init__(self):
        self.one_shot = True

    def read(self,file_name):
        t = []
        v = []
        t_accum = 0
        with open(file_name,'rb') as f:
            while True:
                t_val = f.read(LEN_TIME_FIELDS)
                v_val = f.read(LEN_VLUE_FIELDS)
                if not t_val:
                    break
                t_accum += int.from_bytes(t_val,"big")
                t.append(t_accum)
                v.append(int.from_bytes(v_val,"big", signed=True))
        return t,v

    def write(self,t,v,file_name):
        with open(file_name,'wb') as f:
            f.write(t[0].to_bytes(LEN_TIME_FIELDS,'big'))
            f.write(v[0].to_bytes(LEN_VLUE_FIELDS,'big', signed=True))
            for i in range(1,len(t)):
                '''if t[i]-t[i-1] > 2**8:
                    print(t[i]," ---> ",t[i]-t[i-1])
                if t[i] == 102077:
                    print(t[i]," - ",t[i-1]," ---> ",t[i]-t[i-1])'''
                f.write((t[i]-t[i-1]).to_bytes(LEN_TIME_FIELDS,'big'))
                f.write(v[i].to_bytes(LEN_VLUE_FIELDS,'big', signed=True))
        
    def read_tagged(self,file_name):
        '''
        File will 4 bytes long tag, after,
        5 bytes, tell the length of the current section (max (1e3)e9 values)
        '''
        with open(file_name,'rb') as f:
            #Init meta fields
            tag_mode = True
            tag = None
            len_section = None
            section_pos = None
            #Init data fields
            data = {} # "$tag":{"t":[],"v":[]}
            t_accum = None
            t = None
            v = None
            while True:
                if tag_mode:
                    tag = f.read(LEN_TAG).decode('utf-8')
                    len_section = int.from_bytes(f.read(LEN_META_SECTION_SIZE),"big")
                    tag_mode = False
                    section_pos = 0

                    t_accum = 0
                    t = []
                    v = []

                t_val = f.read(LEN_TIME_FIELDS)
                v_val = f.read(LEN_VLUE_FIELDS)
                if not t_val:
                    break
                t_accum += int.from_bytes(t_val,"big")
                t.append(t_accum)
                v.append(int.from_bytes(v_val,"big", signed=True))
                section_pos += 1
                if section_pos == len_section:
                    data[tag] = {"t":t,"v":v}
                    tag_mode = True
        return data

    def write_tagged(self,data_dict,file_name):
        '''
        data dict is in the form: data[tag] = {"t":t,"v":v}
        File will 4 bytes long tag, after,
        5 bytes, tell the length of the current section (max (1e3)e9 values)
        '''
        with open(file_name,'wb') as f:
            for k in data_dict.keys():
                section_len = len(data_dict[k]['t'])
                f.write(self.__get_tag_alligned(k).encode('utf-8'))
                f.write(section_len.to_bytes(5,"big"))
                t = data_dict[k]['t']
                v = data_dict[k]['v']

                f.write(t[0].to_bytes(LEN_TIME_FIELDS,'big'))
                f.write(v[0].to_bytes(LEN_VLUE_FIELDS,'big', signed=True))
                for i in range(1,len(t)):
                    f.write((t[i]-t[i-1]).to_bytes(LEN_TIME_FIELDS,'big'))
                    f.write(v[i].to_bytes(LEN_VLUE_FIELDS,'big', signed=True))

    def __get_tag_alligned(self,tag):
        str_tag = str(tag)
        if len(str_tag) > LEN_TAG:
            str_tag = str_tag[0:LEN_TAG]
        elif len(str_tag) < LEN_TAG:
            delta = LEN_TAG - len(str_tag)
            str_tag = " "*delta+str_tag 
        return str_tag

if __name__ == "__main__":
    path = "./temp"
    path_dict = "./temp_dict"
    t = [1,2,3,4,5,10,200]
    v = [11,22,33,44,55,7,2]
    
    data_dict = {"first":{"t":[1,2,3,4,54,67,78,79,81], "v":[11,12,13,14,154,167,178,179,181]},
                 "sec":{"t":[1,2,3,4,14,32,36,42,53], "v":[5,5,5,5,5,5,5,5,5]},
                 "8bit":{"t":[11,12,13,14,114,132,136,142,153], "v":[55,75,25,5,55,65,51,52,57]}}

    manager = csvManager()

    manager.write(t,v,path)
    t_readed,v_readed = manager.read(path)

    manager.write_tagged(data_dict,path_dict)
    data_dict_readed = manager.read_tagged(path_dict)

    print(t_readed,v_readed)
    print(data_dict_readed)

